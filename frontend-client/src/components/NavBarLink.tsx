import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

interface NavBarLink {
  title: string;
  to: string;
  onClick: () => void;
  className: string;
  icon: ReactElement;
}

const NavBarLink = ({
  title,
  to,
  onClick,
  className,
  icon,
}: NavBarLink): ReactElement => (
  <Link className={`nav-item ${className}`} to={to} onClick={onClick}>
    <span className='nav-item-icon'>{icon}</span>
    {title}
  </Link>
);

export default NavBarLink;
