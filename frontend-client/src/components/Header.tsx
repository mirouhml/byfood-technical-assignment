import React, { ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import NavBarLink from './NavBarLink';
import { AppDispatch, RootState } from '../app/store';
import { setFormType } from '../features/users/slices/applicationStateSlice';
import { FormType } from '../types';
import { FaUserTimes, FaUserEdit, FaUserPlus } from 'react-icons/fa';
import '../assets/header.css';

const Header = (): ReactElement => {
  const dispatch = useDispatch<AppDispatch>();
  const { userIsSelected } = useSelector(
    (state: RootState) => state.applicationState
  );
  const dispatchSetFormType = (formType: FormType) =>
    dispatch(setFormType(formType));
  const location = useLocation();

  if (location.pathname === '/')
    return (
      <header className='main-header'>
        <ul className='main-header-list'>
          <li className='logo'>
            <h1>Users</h1>
          </li>
          <li>
            <div className='vl'></div>
          </li>
          <li>
            <NavBarLink
              title='Add'
              to={'/new'}
              onClick={() => dispatchSetFormType('Add')}
              className='active-link'
              icon={<FaUserPlus size={20} />}
            />
          </li>
          <li>
            <NavBarLink
              title='Edit'
              to='/edit'
              onClick={() => dispatchSetFormType('Edit')}
              className={userIsSelected ? 'active-link' : 'disabled-link'}
              icon={<FaUserEdit size={20} />}
            />
          </li>{' '}
          <li>
            <NavBarLink
              title='Delete'
              to='/delete'
              onClick={() => dispatchSetFormType('Delete')}
              className={userIsSelected ? 'active-link' : 'disabled-link'}
              icon={<FaUserTimes size={20} />}
            />
          </li>
        </ul>
      </header>
    );
  return <header></header>;
};

export default Header;
