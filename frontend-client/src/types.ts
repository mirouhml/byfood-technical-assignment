export type User = {
  ID: string;
  Name: string;
  Email: string;
  CreatedAt: string;
  UpdatedAt: string;
};

export type FormType = 'Add' | 'Edit' | 'Delete';

export type APIError = {
  ID: string | undefined;
  message: string;
};

export type APIResponse = User | APIError;
