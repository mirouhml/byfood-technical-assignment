import { User } from './types';

export const EMPTY_USER: User = {
  ID: '',
  Name: '',
  Email: '',
  CreatedAt: '',
  UpdatedAt: '',
};
