import React, { useEffect, ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsersAsync } from '../../slices/userOperationsSlice';
import {
  setSelectedUser,
  clearSelectedUser,
} from '../../slices/applicationStateSlice';
import { AppDispatch, RootState } from '../../../../app/store';
import { User } from '../../../../types';
import UserRow from '../user/UserRow';

const UsersList = (): ReactElement => {
  const dispatch = useDispatch<AppDispatch>();
  const { status, users } = useSelector((state: RootState) => state.users);
  const { selectedUser, userIsSelected } = useSelector(
    (state: RootState) => state.applicationState
  );
  const dispatchSetSelectedUser = (user: User) =>
    dispatch(setSelectedUser(user));
  const dispatchClearSelectedUser = () => dispatch(clearSelectedUser());
  useEffect(() => {
    if (status.fetchUsers === 'idle') {
      dispatch(fetchUsersAsync());
    }

    if (!userIsSelected && selectedUser) {
      dispatchClearSelectedUser();
    }
  }, []);

  const onRowClick = (user: User) => {
    if (selectedUser?.ID === user.ID) {
      dispatchClearSelectedUser();
      return;
    }
    dispatchSetSelectedUser(user);
  };
  return (
    <table>
      <thead>
        <tr>
          <th>Full name</th>
          <th>Email</th>
          <th>Created At</th>
          <th>Updated At</th>
        </tr>
      </thead>
      <tbody>
        {status.fetchUsers === 'succeeded' &&
          users.map((user) => (
            <UserRow
              key={user.ID}
              user={user}
              onClick={() => onRowClick(user)}
              selected={selectedUser?.ID === user.ID ? true : false}
            />
          ))}
        {status.fetchUsers === 'loading' && (
          <tr className='error-row'>
            <td colSpan={4}>Loading...</td>
          </tr>
        )}
        {status.fetchUsers === 'failed' && (
          <tr className='error-row'>
            <td colSpan={4}>Failed to load users</td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export default UsersList;
