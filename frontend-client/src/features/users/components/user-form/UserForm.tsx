import React, { ReactElement, useState, useEffect, useRef } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import userOperationsSlice from '../../slices/userOperationsSlice';
import {
  createUserAsync,
  editUserAsync,
  deleteUserAsync,
} from '../../slices/userOperationsSlice';
import { clearSelectedUser } from '../../slices/applicationStateSlice';
import { AppDispatch, RootState } from '../../../../app/store';
import { User, FormType } from '../../../../types';
import { IoIosArrowBack } from 'react-icons/io';
import Input from './Input';

const UserForm = (): ReactElement => {
  const dispatch = useDispatch<AppDispatch>();
  const { selectedUser, userIsSelected, formType } = useSelector(
    (state: RootState) => state.applicationState
  );
  const dispatchClearSelectedUser = () => dispatch(clearSelectedUser());
  const dispatchResetFormStatus = () =>
    dispatch(userOperationsSlice.actions.resetFormStatus());
  const [user, setUser] = useState<User>(selectedUser);
  const { status, error } = useSelector((state: RootState) => state.users);
  const navigate = useNavigate();
  const submitButtonTitle: { [key in FormType]: string } = {
    Add: 'Create',
    Edit: 'Save',
    Delete: 'Delete',
  };
  const buttonRef = useRef<HTMLButtonElement>(null);

  useEffect(
    () => () => {
      dispatchResetFormStatus();
    },
    []
  );

  useEffect(() => {
    if (!userIsSelected && formType !== 'Add') {
      navigate('/');
    }
  }, []);

  useEffect(() => {
    if (status.createUser === 'failed') {
      toast.error(error);
    } else if (status.createUser === 'succeeded') {
      toast.success('User created successfully!');
      navigate('/');
    } else if (status.editUser === 'failed') {
      toast.error(error);
    } else if (status.editUser === 'succeeded') {
      toast.success('User edited successfully!');
      navigate('/');
    } else if (status.deleteUser === 'failed') {
      toast.error(error);
    } else if (status.deleteUser === 'succeeded') {
      toast.success('User deleted successfully!');
      navigate('/');
    }
  }, [status]);

  const saveUserOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    switch (formType) {
      case 'Add':
        dispatch(createUserAsync(user));
        break;
      case 'Edit':
        dispatch(editUserAsync(user));
        break;
      case 'Delete':
        dispatch(deleteUserAsync(user.ID));
        break;
      default:
        break;
    }
    dispatchClearSelectedUser();
  };

  return (
    <form onSubmit={(e) => handleSubmit(e)} className='user-form'>
      <div className='form-header'>
        <Link
          to={'..'}
          onClick={() => dispatchClearSelectedUser()}
          className='form-back'
        >
          <IoIosArrowBack />
          Back
        </Link>
        <h1 className='form-title'>{formType} User</h1>
      </div>
      <Input
        id='name'
        name='Name'
        value={formType === 'Add' ? '' : user?.Name}
        type='text'
        onChange={saveUserOnChange}
      />
      <Input
        id='email'
        name='Email'
        value={formType === 'Add' ? '' : user?.Email}
        type='text'
        onChange={saveUserOnChange}
      />
      <button
        className={`form-button ${
          user.Name === selectedUser.Name &&
          user.Email === selectedUser.Email &&
          formType === 'Edit'
            ? 'disabled-button'
            : 'active-button'
        }`}
        type='submit'
        ref={buttonRef}
      >
        {formType && submitButtonTitle[formType]}
      </button>
    </form>
  );
};
export default UserForm;
