import React, { ReactElement } from 'react';

interface InputProps {
  id: string;
  name: string;
  value: string;
  type: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input = ({
  id,
  name,
  value,
  type,
  onChange,
}: InputProps): ReactElement => {
  return (
    <div className='form-input'>
      <input
        id={id}
        name={name}
        defaultValue={value}
        type={type}
        onChange={onChange}
        className='input-text'
        required
      />
      <span className='floating-label'>{name}</span>
    </div>
  );
};

export default Input;
