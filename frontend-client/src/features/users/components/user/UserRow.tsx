import React, { ReactElement } from 'react';

import { User } from '../../../../types';

interface UserRowProps {
  user: User;
  onClick: () => void;
  selected: boolean;
}
const UserRow = ({ user, onClick, selected }: UserRowProps): ReactElement => {
  return (
    <tr
      onClick={onClick}
      className={`user-row ${selected ? 'is-selected' : ''}`}
    >
      <td>{user.Name}</td>
      <td>{user.Email}</td>
      <td>{new Date(user.CreatedAt).toLocaleString()}</td>
      <td>{new Date(user.UpdatedAt).toLocaleString()}</td>
    </tr>
  );
};

export default UserRow;
