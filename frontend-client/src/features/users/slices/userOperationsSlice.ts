import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  fetchUsers,
  createUser,
  editUser,
  deleteUser,
} from '../services/usersAPI';
import { User, APIResponse, APIError } from '../../../types';

interface UsersState {
  users: User[];
  status: {
    fetchUsers: 'idle' | 'loading' | 'succeeded' | 'failed';
    createUser: 'idle' | 'loading' | 'succeeded' | 'failed';
    editUser: 'idle' | 'loading' | 'succeeded' | 'failed';
    deleteUser: 'idle' | 'loading' | 'succeeded' | 'failed';
  };
  error: string | null | undefined;
}

const initialState: UsersState = {
  users: [],
  status: {
    fetchUsers: 'idle',
    createUser: 'idle',
    editUser: 'idle',
    deleteUser: 'idle',
  },
  error: null,
};

export const fetchUsersAsync = createAsyncThunk(
  'users/fetchUsers',
  async () => {
    const response = await fetchUsers();
    return response;
  }
);

export const createUserAsync = createAsyncThunk(
  'user/createUser',
  async (user: User) => {
    const response = await createUser(user);
    return response;
  }
);

export const editUserAsync = createAsyncThunk(
  'user/editUser',
  async (user: User) => {
    const response = await editUser(user);
    return response;
  }
);

export const deleteUserAsync = createAsyncThunk(
  'user/deleteUser',
  async (id: string) => {
    const response = await deleteUser(id);
    return response;
  }
);

function isAPIError(toBeDetermined: APIResponse): toBeDetermined is APIError {
  return (toBeDetermined as APIError).message !== undefined;
}

const userOperationsSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    resetFormStatus: (state) => {
      state.status = {
        fetchUsers: state.status.fetchUsers,
        createUser: 'idle',
        editUser: 'idle',
        deleteUser: 'idle',
      };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsersAsync.pending, (state) => {
        state.status.fetchUsers = 'loading';
      })
      .addCase(
        fetchUsersAsync.fulfilled,
        (state, action: PayloadAction<User[]>) => {
          if (action.payload.length === 0) {
            state.status.fetchUsers = 'failed';
            return;
          }
          state.status.fetchUsers = 'succeeded';
          state.users = action.payload;
        }
      )
      .addCase(fetchUsersAsync.rejected, (state, action) => {
        state.status.fetchUsers = 'failed';
        state.error = action.error.message;
      })
      .addCase(createUserAsync.pending, (state) => {
        state.status.createUser = 'loading';
      })
      .addCase(
        createUserAsync.fulfilled,
        (state, action: PayloadAction<APIResponse>) => {
          if (isAPIError(action.payload)) {
            state.status.createUser = 'failed';
            state.error = action.payload.message;
            return;
          } else if (action.payload instanceof Object) {
            state.status.createUser = 'succeeded';
            state.users = [...state.users, action?.payload];
            return;
          }
        }
      )
      .addCase(createUserAsync.rejected, (state, action) => {
        state.status.createUser = 'failed';
        state.error = action.error.message;
      })
      .addCase(editUserAsync.pending, (state) => {
        state.status.editUser = 'loading';
      })
      .addCase(
        editUserAsync.fulfilled,
        (state, action: PayloadAction<APIResponse>) => {
          if (isAPIError(action.payload)) {
            state.status.editUser = 'failed';
            state.error = action.payload.message;
            return;
          } else if (action.payload instanceof Object) {
            state.status.editUser = 'succeeded';
            const index = state.users.findIndex(
              (user) => user.ID === action.payload?.ID
            );
            state.users[index] = action.payload;
          }
        }
      )
      .addCase(editUserAsync.rejected, (state, action) => {
        state.status.editUser = 'failed';
        state.error = action.error.message;
      })
      .addCase(deleteUserAsync.pending, (state) => {
        state.status.deleteUser = 'loading';
      })
      .addCase(
        deleteUserAsync.fulfilled,
        (state, action: PayloadAction<APIResponse>) => {
          if (isAPIError(action.payload)) {
            state.status.deleteUser = 'failed';
            state.error = action.payload.message;
            return;
          }
          state.status.deleteUser = 'succeeded';
          state.users = state.users.filter(
            (user) => user.ID !== action.payload?.ID
          );
        }
      )
      .addCase(deleteUserAsync.rejected, (state, action) => {
        state.status.deleteUser = 'failed';
        state.error = action.error.message;
      });
  },
});

export default userOperationsSlice;
