import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User, FormType } from '../../../types';
import { EMPTY_USER } from '../../../constants';

interface UsersState {
  selectedUser: User;
  userIsSelected: boolean;
  formType: FormType | undefined;
}

const initialState: UsersState = {
  selectedUser: EMPTY_USER,
  userIsSelected: false,
  formType: undefined,
};

export const applicationState = createSlice({
  name: 'applicationState',
  initialState,
  reducers: {
    setSelectedUser(state, action: PayloadAction<User>) {
      state.selectedUser = action.payload;
      state.userIsSelected = true;
    },
    setFormType(state, action: PayloadAction<FormType>) {
      state.formType = action.payload;
      if (action.payload === 'Add') {
        state.selectedUser = EMPTY_USER;
      }
    },
    clearSelectedUser(state) {
      state.selectedUser = EMPTY_USER;
      state.userIsSelected = false;
    },
  },
});

export const { setSelectedUser, setFormType, clearSelectedUser } =
  applicationState.actions;

export default applicationState.reducer;
