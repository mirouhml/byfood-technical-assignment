import { User, APIResponse } from '../../../types';
export const fetchUsers = async (): Promise<User[]> => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/api/users/`,
      {
        method: 'GET',
      }
    );
    const users = await response.json();
    return users;
  } catch (error) {
    return [];
  }
};

export const createUser = async (user: User): Promise<APIResponse> => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/api/users/`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: user.Name,
          email: user.Email,
        }),
      }
    );
    if (response.status === 409) {
      return {
        ID: '',
        message: 'User already exists!',
      };
    }
    const newUser = await response.json();
    return newUser;
  } catch (error) {
    return {
      ID: '',
      message: (error as Error).message,
    };
  }
};

export const editUser = async (user: User): Promise<APIResponse> => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/api/users/${user.ID}`,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
      }
    );
    if (response.status === 404) {
      return {
        ID: '',
        message: 'User not found!',
      };
    }
    return user;
  } catch (error) {
    return {
      ID: '',
      message: (error as Error).message,
    };
  }
};

export const deleteUser = async (id: string): Promise<APIResponse> => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/api/users/${id}`,
      {
        method: 'DELETE',
      }
    );
    if (response.status === 404) {
      return {
        ID: '',
        message: 'User not found!',
      };
    }
    return {
      ID: id,
    } as User;
  } catch (error) {
    return {
      ID: '',
      message: (error as Error).message,
    };
  }
};
