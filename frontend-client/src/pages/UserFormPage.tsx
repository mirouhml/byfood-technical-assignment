import React, { ReactElement } from 'react';
import UserForm from '../features/users/components/user-form/UserForm';
import '../assets/user-form-page.css';

const UserFormPage = (): ReactElement => (
  <div className='user-form-page'>
    <UserForm />
  </div>
);

export default UserFormPage;
