import React, { ReactElement } from 'react';
import UsersList from '../features/users/components/users-list/UsersList';
import '../assets/users-page.css';

const UsersPage = (): ReactElement => (
  <div className='users-page'>
    <UsersList />
  </div>
);

export default UsersPage;
