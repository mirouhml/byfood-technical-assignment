import React from 'react';
import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import UsersPage from './pages/UsersPage';
import UserFormPage from './pages/UserFormPage';
import Header from './components/Header';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className='app'>
      <BrowserRouter>
        <Header />
        <main className='main-container'>
          <Routes>
            <Route path='/' element={<Outlet />}>
              <Route index element={<UsersPage />} />
              <Route path=':operation' element={<UserFormPage />} />
            </Route>
          </Routes>
        </main>
      </BrowserRouter>
      <aside>
        <ToastContainer
          position='bottom-right'
          autoClose={3000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </aside>
    </div>
  );
}

export default App;
