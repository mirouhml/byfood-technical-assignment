import { configureStore } from '@reduxjs/toolkit';

import userOperationsSlice from '../features/users/slices/userOperationsSlice';
import applicationStateReducer from '../features/users/slices/applicationStateSlice';

const store = configureStore({
  reducer: {
    users: userOperationsSlice.reducer,
    applicationState: applicationStateReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
