package main

import (
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/controllers"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/initializers"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/routes"
)

var (
	server *gin.Engine

	UserController      controllers.UserController
	UserRouteController routes.UserRouteController
)

func init() {
	config, err := initializers.LoadConfig(".")
	if err != nil {
		log.Fatal("? Could not load environment variables", err)
	}

	initializers.ConnectDB(&config)

	UserController = controllers.NewUserController(initializers.DB)
	UserRouteController = routes.NewUserRouteController(UserController)

	server = gin.Default()
	server.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders: []string{"Origin", "Content-Length", "Content-Type"},
	}))
}

func main() {
	config, err := initializers.LoadConfig(".")
	if err != nil {
		log.Fatal("? Could not load environment variables", err)
	}

	router := server.Group("/api")
	router.GET("/", func(ctx *gin.Context) {
		message := "Welcome to our API!!"
		ctx.JSON(http.StatusOK, gin.H{"status": "success", "message": message})
	})

	UserRouteController.UserRoute(router)

	log.Fatal(server.Run(":" + config.ServerPort))
}
