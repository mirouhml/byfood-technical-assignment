package controllers

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/models"
	"gorm.io/gorm"
)

type UserController struct {
	DB *gorm.DB
}

type NewUser struct {
	Name  string `json:"name" binding:"required"`
	Email string `json:"email" binding:"required"`
}

type UserUpdate struct {
	Name      string `json:"name"`
	Email     string `json:"email"`
	UpdatedAt time.Time
}

func NewUserController(DB *gorm.DB) UserController {
	return UserController{DB}
}

func (uc *UserController) CreateUser(ctx *gin.Context) {
	var newUser NewUser

	if err := ctx.ShouldBindJSON(&newUser); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	result := uc.DB.Select("Email").Where("Email = ?", newUser.Email).First(&models.User{})
	if result.Error == nil {
		ctx.JSON(http.StatusConflict, gin.H{"error": "Email already exists!"})
		return
	}

	user := models.User{
		Name:      newUser.Name,
		Email:     newUser.Email,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	result = uc.DB.Create(&user)
	if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusCreated, user)
}

func (uc *UserController) UpdateUser(ctx *gin.Context) {
	userId := ctx.Param("userId")

	var newUser UserUpdate

	if err := ctx.ShouldBindJSON(&newUser); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	result := uc.DB.Select("ID").Where("ID = ?", userId).First(&models.User{})
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		ctx.JSON(http.StatusNotFound, gin.H{"error": "User not found!"})
		return
	} else if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	userToUpdate := UserUpdate{
		Name:      newUser.Name,
		Email:     newUser.Email,
		UpdatedAt: time.Now(),
	}

	result = uc.DB.Model(&models.User{}).Where("id = ?", userId).
		Select("Name", "Email", "UpdatedAt").
		Updates(map[string]interface{}{
			"Name":      userToUpdate.Name,
			"Email":     userToUpdate.Email,
			"UpdatedAt": userToUpdate.UpdatedAt,
		})

	if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusNoContent, nil)
}

func (uc *UserController) GetUsers(ctx *gin.Context) {
	var users []models.User

	result := uc.DB.Find(&users)

	if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusOK, users)
}

func (uc *UserController) GetUser(ctx *gin.Context) {
	userId := ctx.Param("userId")

	var user models.User

	result := uc.DB.First(&user, "id = ?", userId)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		ctx.JSON(http.StatusNotFound, gin.H{"error": "User not found!"})
		return
	} else if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusOK, user)
}

func (uc *UserController) DeleteUser(ctx *gin.Context) {
	userId := ctx.Param("userId")

	result := uc.DB.Select("ID").Where("ID = ?", userId).First(&models.User{})
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		ctx.JSON(http.StatusNotFound, gin.H{"error": "User not found!"})
		return
	} else if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	result = uc.DB.Delete(&models.User{}, "id = ?", userId)

	if result.Error != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": result.Error.Error()})
		return
	}

	ctx.IndentedJSON(http.StatusNoContent, nil)
}
