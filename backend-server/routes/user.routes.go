package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/controllers"
)

type UserRouteController struct {
	userController controllers.UserController
}

func NewUserRouteController(userController controllers.UserController) UserRouteController {
	return UserRouteController{userController}
}

func (uc *UserRouteController) UserRoute(rg *gin.RouterGroup) {
	router := rg.Group("users")
	router.POST("/", uc.userController.CreateUser)
	router.PUT("/:userId", uc.userController.UpdateUser)
	router.GET("/", uc.userController.GetUsers)
	router.GET("/:userId", uc.userController.GetUser)
	router.DELETE("/:userId", uc.userController.DeleteUser)
}
