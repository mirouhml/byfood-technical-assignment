package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/initializers"
	"gorm.io/gorm"
)

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

var DB *gorm.DB

func init() {
	config, err := initializers.LoadConfig(".")
	if err != nil {
		log.Fatal("? Could not load environment variables", err)
	}

	initializers.ConnectDB(&config)
	DB = initializers.DB
}

func main() {

	jsonFile, err := os.ReadFile("./seed/users.json")
	if err != nil {
		fmt.Println(err)
		return
	}

	var users []User

	err = json.Unmarshal(jsonFile, &users)

	if err != nil {
		fmt.Println(err)
		return
	}

	var result *gorm.DB

	for i := 0; i < len(users); i++ {
		user := users[i]
		result = DB.Create(&user)
	}

	if result.Error == nil {
		fmt.Println("The database has been seeded!")
	} else {
		fmt.Println("There is an error with seeding!")
	}
}
