package main

import (
	"fmt"
	"log"

	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/initializers"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/models"
)

func init() {
	config, err := initializers.LoadConfig(".")
	if err != nil {
		log.Fatal("? Could not load environment variables", err)
	}

	initializers.ConnectDB(&config)
}

func main() {
	initializers.DB.AutoMigrate(&models.User{})
	fmt.Println("? Migration complete")
}
