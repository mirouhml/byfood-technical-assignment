package controllers

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/controllers"
	"gitlab.com/mirouhml/bygood-technical-assignment/backend-server/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type AnyTime struct{}

type UserControllerTestSuite struct {
	suite.Suite
	db   sql.DB
	mock sqlmock.Sqlmock
	uc   controllers.UserController
}

var user1 = models.User{
	ID:        uuid.New(),
	Name:      "John Doe",
	Email:     "john.doe@example.com",
	CreatedAt: time.Now(),
	UpdatedAt: time.Now(),
}

var user2 = models.User{
	ID:        uuid.New(),
	Name:      "Jane Doe",
	Email:     "jane.doe@example.com",
	CreatedAt: time.Now(),
	UpdatedAt: time.Now(),
}

// Match satisfies sqlmock.Argument interface
func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func init() {
	gin.SetMode(gin.TestMode)
}

func (suite *UserControllerTestSuite) SetupTest() {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}
	var gdb *gorm.DB
	gdb, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{})
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	suite.db = *db
	suite.uc = controllers.NewUserController(gdb)
	suite.mock = mock
}

func (suite *UserControllerTestSuite) TearDownTest() {
	suite.mock.ExpectClose()
	suite.db.Close()
}

func (suite *UserControllerTestSuite) TestCreateUser() {
	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT \"email\" FROM \"users\" WHERE Email = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.Email).
		WillReturnRows(sqlmock.NewRows([]string{"email"}))

	suite.mock.ExpectBegin()

	rows := sqlmock.NewRows([]string{"id", "name", "email", "created_at", "updated_at"}).
		AddRow(user1.ID, user1.Name, user1.Email, user1.CreatedAt, user1.UpdatedAt)

	suite.mock.ExpectQuery(regexp.QuoteMeta("INSERT INTO \"users\" (\"name\",\"email\",\"created_at\",\"updated_at\") VALUES ($1,$2,$3,$4) RETURNING \"id\",\"created_at\",\"updated_at\"")).
		WithArgs(user1.Name, user1.Email, AnyTime{}, AnyTime{}).
		WillReturnRows(rows)
	suite.mock.ExpectCommit()

	router := gin.Default()
	router.POST("/users", suite.uc.CreateUser)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/users", bytes.NewBuffer([]byte(`{"name":"John Doe","email":"john.doe@example.com" }`)))
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 201)
	assert.JSONEq(suite.T(), w.Body.String(), fmt.Sprintf(`{"ID": "%s","Name":"%s","Email":"%s","CreatedAt":"%s","UpdatedAt":"%s"}`, user1.ID, user1.Name, user1.Email, user1.CreatedAt.Format(time.RFC3339Nano), user1.UpdatedAt.Format(time.RFC3339Nano)))

	if err := suite.mock.ExpectationsWereMet(); err != nil {
		suite.T().Errorf("there were unfulfilled expectations: %s", err)
	}
}

func (suite *UserControllerTestSuite) TestCreateUserFailed() {
	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT \"email\" FROM \"users\" WHERE Email = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.Email).
		WillReturnRows(sqlmock.NewRows([]string{"email"}).AddRow(user1.Email))

	suite.mock.ExpectBegin()

	rows := sqlmock.NewRows([]string{"id", "name", "email", "created_at", "updated_at"}).
		AddRow(user1.ID, user1.Name, user1.Email, user1.CreatedAt, user1.UpdatedAt)

	suite.mock.ExpectQuery(regexp.QuoteMeta("INSERT INTO \"users\" (\"name\",\"email\",\"created_at\",\"updated_at\") VALUES ($1,$2,$3,$4) RETURNING \"id\",\"created_at\",\"updated_at\"")).
		WithArgs(user1.Name, user1.Email, AnyTime{}, AnyTime{}).
		WillReturnRows(rows)
	suite.mock.ExpectCommit()

	router := gin.Default()
	router.POST("/users", suite.uc.CreateUser)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/users", bytes.NewBuffer([]byte(`{"name":"John Doe","email":"john.doe@example.com" }`)))
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 409)
	assert.JSONEq(suite.T(), w.Body.String(), `{"error":"Email already exists!"}`)
}

func (suite *UserControllerTestSuite) TestGetUsers() {
	rows := sqlmock.NewRows([]string{"id", "name", "email", "created_at", "updated_at"}).
		AddRow(user1.ID, user1.Name, user1.Email, user1.CreatedAt, user1.UpdatedAt).
		AddRow(user2.ID, user2.Name, user2.Email, user2.CreatedAt, user2.UpdatedAt)

	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM \"users\"")).
		WillReturnRows(rows)

	router := gin.Default()
	router.GET("/users", suite.uc.GetUsers)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/users", nil)
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 200)
	assert.JSONEq(suite.T(), w.Body.String(), fmt.Sprintf(`[{"ID": "%s","Name":"%s","Email":"%s","CreatedAt":"%s","UpdatedAt":"%s"},{"ID": "%s","Name":"%s","Email":"%s","CreatedAt":"%s","UpdatedAt":"%s"}]`, user1.ID, user1.Name, user1.Email, user1.CreatedAt.Format(time.RFC3339Nano), user1.UpdatedAt.Format(time.RFC3339Nano), user2.ID, user2.Name, user2.Email, user2.CreatedAt.Format(time.RFC3339Nano), user2.UpdatedAt.Format(time.RFC3339Nano)))

	if err := suite.mock.ExpectationsWereMet(); err != nil {
		suite.T().Errorf("there were unfulfilled expections: %s", err)
	}

}

func (suite *UserControllerTestSuite) TestGetUserFailed() {
	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM \"users\" WHERE id = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.ID).
		WillReturnRows(sqlmock.NewRows([]string{}))

	router := gin.Default()
	router.GET("/users/:userId", suite.uc.GetUser)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/users/%s", user1.ID), nil)
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 404)
	assert.JSONEq(suite.T(), w.Body.String(), `{"error":"User not found!"}`)

}

func (suite *UserControllerTestSuite) TestUpdateUser() {
	rows := sqlmock.NewRows([]string{"id", "name", "email", "created_at", "updated_at"}).
		AddRow(user1.ID, user1.Name, user1.Email, user1.CreatedAt, user1.UpdatedAt)

	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT \"id\" FROM \"users\" WHERE ID = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.ID).
		WillReturnRows(rows)

	suite.mock.ExpectBegin()
	suite.mock.ExpectExec(regexp.QuoteMeta("UPDATE \"users\" SET \"email\"=$1,\"name\"=$2,\"updated_at\"=$3 WHERE id = $4")).
		WithArgs("jane.doe@example.com", "Jane Doe", AnyTime{}, user1.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	suite.mock.ExpectCommit()

	router := gin.Default()
	router.PUT("/users/:userId", suite.uc.UpdateUser)

	var jsonStr = []byte(`{"name":"Jane Doe","email":"jane.doe@example.com"}`)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", fmt.Sprintf("/users/%s", user1.ID), bytes.NewBuffer(jsonStr))
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 204)

	if err := suite.mock.ExpectationsWereMet(); err != nil {
		suite.T().Errorf("there were unfulfilled expections: %s", err)
	}
}

func (suite *UserControllerTestSuite) TestUpdateUserFailed() {
	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT \"id\" FROM \"users\" WHERE ID = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.ID).
		WillReturnRows(sqlmock.NewRows([]string{}))

	suite.mock.ExpectBegin()
	suite.mock.ExpectExec(regexp.QuoteMeta("UPDATE \"users\" SET \"email\"=$1,\"name\"=$2,\"updated_at\"=$3 WHERE id = $4")).
		WithArgs("jane.doe@example.com", "Jane Doe", AnyTime{}, user1.ID).
		WillReturnResult(sqlmock.NewResult(1, 0))
	suite.mock.ExpectCommit()

	router := gin.Default()
	router.PUT("/users/:userId", suite.uc.UpdateUser)

	var jsonStr = []byte(`{"name":"Jane Doe","email":"jane.doe@example.com"}`)
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", fmt.Sprintf("/users/%s", user1.ID), bytes.NewBuffer(jsonStr))
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 404)
	assert.JSONEq(suite.T(), w.Body.String(), `{"error":"User not found!"}`)
}

func (suite *UserControllerTestSuite) TestDeleteUser() {
	rows := sqlmock.NewRows([]string{"id", "name", "email", "created_at", "updated_at"}).
		AddRow(user1.ID, user1.Name, user1.Email, user1.CreatedAt, user1.UpdatedAt)

	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT \"id\" FROM \"users\" WHERE ID = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.ID).
		WillReturnRows(rows)

	suite.mock.ExpectBegin()
	suite.mock.ExpectExec(regexp.QuoteMeta("DELETE FROM \"users\" WHERE id = $1")).
		WithArgs(user1.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	suite.mock.ExpectCommit()

	router := gin.Default()
	router.DELETE("/users/:userId", suite.uc.DeleteUser)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", fmt.Sprintf("/users/%s", user1.ID), nil)
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 204)

	if err := suite.mock.ExpectationsWereMet(); err != nil {
		suite.T().Errorf("there were unfulfilled expections: %s", err)
	}
}

func (suite *UserControllerTestSuite) TestDeleteUserFailed() {
	suite.mock.ExpectQuery(regexp.QuoteMeta("SELECT \"id\" FROM \"users\" WHERE ID = $1 ORDER BY \"users\".\"id\" LIMIT 1")).
		WithArgs(user1.ID).
		WillReturnRows(sqlmock.NewRows([]string{}))

	suite.mock.ExpectBegin()
	suite.mock.ExpectExec(regexp.QuoteMeta("DELETE FROM \"users\" WHERE id = $1")).
		WithArgs(user1.ID).
		WillReturnResult(sqlmock.NewResult(0, 0))
	suite.mock.ExpectCommit()

	router := gin.Default()
	router.DELETE("/users/:userId", suite.uc.DeleteUser)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", fmt.Sprintf("/users/%s", user1.ID), nil)
	router.ServeHTTP(w, req)

	assert.Equal(suite.T(), w.Code, 404)
	assert.JSONEq(suite.T(), w.Body.String(), `{"error":"User not found!"}`)
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(UserControllerTestSuite))
}
