package question1

import (
	"reflect"
	"testing"
)

func TestCase1(t *testing.T) {

	input := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf",
		"zc", "lklklklklklklklkl", "l"}

	output := []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd",
		"fdz", "ef", "kf", "zc", "l"}

	result := Sort(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function Sort(%v) FAILED. Expected: %v, result: %v", input, output, result)
	} else {
		t.Logf("Function Sort(%v) PASSED. Expected: %v, result: %v", input, output, result)
	}
}

func TestCase2(t *testing.T) {

	input := []string{"aab", "aa", "aaaadb", "aaabcd", "efgs", "csssad", "abd"}

	output := []string{"aaaadb", "aaabcd", "aab", "aa", "csssad", "abd", "efgs"}

	result := Sort(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function Sort(%v) FAILED. Expected: %v, result: %v", input, output, result)
	} else {
		t.Logf("Function Sort(%v) PASSED. Expected: %v, result: %v", input, output, result)
	}
}

func TestCase3(t *testing.T) {

	input := []string{"abd", "abcd", "a", "ab", "abdse"}

	output := []string{"abdse", "abcd", "abd", "ab", "a"}

	result := Sort(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function Sort(%v) FAILED. Expected: %v, result: %v", input, output, result)
	} else {
		t.Logf("Function Sort(%v) PASSED. Expected: %v, result: %v", input, output, result)
	}
}

func TestCase4(t *testing.T) {

	input := []string{}

	output := []string{}

	result := Sort(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function Sort(%v) FAILED. Expected: %v, result: %v", input, output, result)
	} else {
		t.Logf("Function Sort(%v) PASSED. Expected: %v, result: %v", input, output, result)
	}
}
