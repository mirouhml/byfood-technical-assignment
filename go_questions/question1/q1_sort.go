package question1

import (
	"sort"
	"strings"
)

// Sort function sorts a given array of words using sort.Slice method by giving it a custom comparator and following the conditions provided:
// - Sorting by the number of character "a"s within the word in a decreasing order.
// - If some words contain the same amount of character "a"s then they are sorted by their lengths.

func Sort(array []string) []string {
	sort.Slice(array, func(i, j int) bool {
		word1, word2 := array[i], array[j]
		count1, count2 := strings.Count(word1, "a"), strings.Count(word2, "a")
		if count1 != count2 {
			return count1 > count2
		}
		return len(word1) > len(word2)
	})

	return array
}
