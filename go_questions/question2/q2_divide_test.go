package question2

import (
	"reflect"
	"testing"
)

func TestCase1(t *testing.T) {

	input := 9

	output := " 2\n 4\n 9\n"

	result := Divide(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function Divide(%v) FAILED. Expected: %s, result: %s", input, output, result)
	} else {
		t.Logf("Function Divide(%v) PASSED. Expected: %s, result: %s", input, output, result)
	}

}

func TestCase2(t *testing.T) {

	input := 0

	output := ""

	result := Divide(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function Divide(%v) FAILED. Expected: %s, result: %s", input, output, result)
	} else {
		t.Logf("Function Divide(%v) PASSED. Expected: %s, result: %s", input, output, result)
	}

}
