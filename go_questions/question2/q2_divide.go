package question2

import "fmt"

// Divide function divides a given number that is greater than 2 by 2 until it reaches 2 and returns the result as a string.
func Divide(number int) string {
	if number == 2 {
		return " 2\n"
	}

	if number < 2 {
		return ""
	}

	return fmt.Sprintln(Divide(number/2), number)
}
