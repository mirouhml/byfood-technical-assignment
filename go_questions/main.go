package main

import (
	"fmt"
	"go_questions/question1"
	"go_questions/question2"
	"go_questions/question3"
)

func main() {
	fmt.Println("Question 1")
	input1 := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	result1 := question1.Sort(input1)
	fmt.Printf("\nThe result of calling the function Sort on the array %+v is: \n%+v \n\n", input1, result1)

	fmt.Println("Question 2")
	input2 := 9
	result2 := question2.Divide(input2)
	fmt.Printf("\nThe result of calling the function Divide on the number %d is: \n%s \n\n", input2, result2)

	fmt.Println("Question 3")
	input3 := []string{"apple", "pie", "apple", "red", "red", "red"}
	result3 := question3.MostRepeated(input3)
	fmt.Printf("\nThe result of calling the function MostRepeated on the dataset %+v is: \n%s \n\n", input3, result3)
}
