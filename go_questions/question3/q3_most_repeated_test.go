package question3

import (
	"reflect"
	"testing"
)

func TestCase1(t *testing.T) {

	input := []string{"apple", "pie", "apple", "red", "red", "red"}

	output := "red"

	result := MostRepeated(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function MostRepeated(%v) FAILED. Expected: %s, result: %s", input, output, result)
	} else {
		t.Logf("Function MostRepeated(%v) PASSED. Expected: %s, result: %s", input, output, result)
	}
}

func TestCase2(t *testing.T) {

	input := []string{"orange", "black", "white", "black", "black", "yellow"}

	output := "black"

	result := MostRepeated(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function MostRepeated(%v) FAILED. Expected: %s, result: %s", input, output, result)
	} else {
		t.Logf("Function MostRepeated(%v) PASSED. Expected: %s, result: %s", input, output, result)
	}
}

func TestCase3(t *testing.T) {

	input := []string{"x", "y", "z", "z", "y", "z", "x"}

	output := "z"

	result := MostRepeated(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function MostRepeated(%v) FAILED. Expected: %s, result: %s", input, output, result)
	} else {
		t.Logf("Function MostRepeated(%v) PASSED. Expected: %s, result: %s", input, output, result)
	}
}

func TestCase4(t *testing.T) {

	input := []string{}

	output := ""

	result := MostRepeated(input)

	if !reflect.DeepEqual(output, result) {
		t.Errorf("Function MostRepeated(%v) FAILED. Expected: %s, result: %s", input, output, result)
	} else {
		t.Logf("Function MostRepeated(%v) PASSED. Expected: %s, result: %s", input, output, result)
	}
}
