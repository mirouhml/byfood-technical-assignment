package question3

// MostRepeated function returns the most repeated string in a given array of strings.
func MostRepeated(array []string) string {

	if len(array) == 0 {
		return ""
	}

	max_repetitions := 0
	result := ""
	repetitions_map := make(map[string]int)

	for i := 0; i < len(array); i++ {
		repetitions_map[array[i]]++

		if max_repetitions <= repetitions_map[array[i]] {
			max_repetitions = repetitions_map[array[i]]
			result = array[i]
		}
	}

	return result
}
