<a name="readme-top"></a>

# byFood-technical-assignment

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [byFood-technical-assignment](#byfood-technical-assignment)
- [📗 Table of Contents](#-table-of-contents)
- [📖 byFood-technical-assignment ](#about-project)
  - [🛠 Built With ](#built-with)
    - [Tech Stack ](#tech-stack)
  - [🚀 Video Demonstration ](#video-demonstration)
  - [💻 Getting Started ](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Setup](#setup)
      - [Backend Instructions](#backend-instructions)
      - [Frontend Instructions](#frontend-instructions)
  - [🔭 Future Features](#future-features)
  - [👥 Authors ](#authors)
  - [🤝 Contributing ](#contributing)
  - [⭐️ Show your support ](#️show-your-support)
  - [📝 License ](#license)

<!-- PROJECT DESCRIPTION -->

# 📖 byFood-technical-assignment <a name="about-project"></a>

This project is my solution to the technical assignment for the byFood company.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

<details>
  <summary>Frontend</summary>
  <ul>
    <li><a href="https://reactjs.org/">React.js</a></li>
    <li><a href="https://redux.js.org/">Redux</a></li>
    <li><a href="https://www.typescriptlang.org/">TypeScript</a></li>
  </ul>
</details>

<details>
  <summary>Backend</summary>
  <ul>
    <li><a href="https://go.dev/">Golang</a></li>
    <li><a href="https://gin-gonic.com/">Gin</a></li>
    <li><a href="https://gorm.io/">Gorm</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.postgresql.org/">PostgreSQL</a></li>
  </ul>
</details>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LIVE DEMO -->

## 🚀 Video Demonstration <a name="video-demonstration"></a>

- [Video Demonstration Link](https://www.loom.com/share/48bec969a33443b7a5263f35509f6c61)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

- Browser
- Golang 1.19
- PostgreSQL
- Node.js 16.13.\*
- npm 9.1.\*

### Setup

Clone this repository to your desired folder:

```sh
  cd my-folder
  git clone git@github.com:myaccount/my-project.git
```

#### Backend Instructions

1. Create a database in PostgreSQL
2. Install uuid extension on PostgreSQL using the following command:

```sh
  CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

3. Create a `app.env` file in the `backend-server` folder and add the following variables:

```sh
  POSTGRES_HOST=127.0.0.1
  POSTGRES_USER=YOUR_USERNAME
  POSTGRES_PASSWORD=YOUR_PASSWORD
  POSTGRES_DB=YOUR_DATABASE_NAME
  POSTGRES_PORT=5432
  PORT=8000
  CLIENT_ORIGIN=http://localhost:3000
```

4. Run the following commands:

```sh
  cd backend-server
  go mod download
```

5. Run the following command to migrate the database:

```sh
  go run migrate/migrate.go
```

6. Run the following command to seed the database:

```sh
  go run seed/seed.go
```

7. Run the following command to start the server:

```sh
  go run main.go
```

Or using:

```sh
  air
```

8. (Optional) Run the following commands to run the tests:

```sh
  cd test
  go test -v
```

#### Frontend Instructions

1. Run the following commands:

```sh
  cd frontend-client
  npm install
```

2. Create a `.env.local` file in the `frontend-client` folder and add the following variables:

```sh
  REACT_APP_API_URL=http://localhost:8000
```

3. Run the following command to start the server:

```sh
  npm start
```

4. Open your browser and go to `http://localhost:3000`

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- FUTURE FEATURES -->

## 🔭 Future Features <a name="future-features"></a>

- [x] **Add automated tests for the API**
- [ ] **Add automated tests for the client app**

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- AUTHORS -->

## 👥 Authors <a name="authors"></a>

👤 **Ammar Hamlaoui**

- GitHub: [@mirouhml](https://github.com/mirouhml)
- LinkedIn: [ammar-hamlaoui](https://www.linkedin.com/in/ammar-hamlaoui/)
- Twitter: [@kuronomirou](https://twitter.com/kuronomirou)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](../../issues/).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- SUPPORT -->

## ⭐️ Show your support <a name="support"></a>

If you like this project STAR it please!

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## 📝 License <a name="license"></a>

This project is [MIT](./LICENSE) licensed.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
